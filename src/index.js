import React from 'react';
import ReactDOM from 'react-dom';
import APP from './app/components/app'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<APP />, document.getElementById('root'));
registerServiceWorker();
