class MiniUrlServiceProxy {
  constructor () {
    this.postUrl = process.env.REACT_APP_POST_ENDPOINT;
  }

  async getMiniUrl(longUrl) {
    console.log(`shortening ${longUrl}`);
    console.log(`Post url is ${this.postUrl}`);

    const response = await fetch(this.postUrl, {
      body: JSON.stringify({url: longUrl}),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST', 
    });
    const data = await response.json();
    console.log('Success:', data);
    return data.shortUrl;
  }
}
export default MiniUrlServiceProxy;