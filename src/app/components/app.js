import React from 'react';
import URLShortner from './url-shortner'
import TinyURLView from './tiny-url-view'
import MiniUrlServiceProxy from '../proxy/mini-url-service-proxy'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.miniUrlService = new MiniUrlServiceProxy();
    this.state = {url: '', tinyUrl: ''};

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTextChange(value) {
    this.setState({url: value})
  }

  async handleSubmit() {
    if (this.state.url.length > 0) {
      const data = await this.miniUrlService.getMiniUrl(this.state.url);
      console.log('The data is ');
      console.log(data);
      this.setState({tinyUrl: data})
    } else {
      alert('Please provide a value');
    }
  }

  render() {
    return (
      <div>
        <URLShortner 
          handleSubmit={this.handleSubmit}
          handleChange={this.handleTextChange}
          textValue={this.state.url}
        />
        <TinyURLView 
          value={this.state.tinyUrl}
        />
      </div>
    );
  }
}

export default App;