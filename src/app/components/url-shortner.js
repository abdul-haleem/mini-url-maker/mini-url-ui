import React from 'react';

class URLShortner extends React.Component {
  constructor(props) {
    super(props);
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.props.handleChange(event.target.value);
  }

  handleSubmit(event) {
    this.props.handleSubmit();
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" value={this.props.textValue} onChange={this.handleChange} />
        <input type="submit" value="Shorten" />
      </form>
    );
  }
}
export default URLShortner;