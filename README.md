# INTRODUCTION
It is a ReactJS app which connects with mini-url-service to shorten a url.

# DEPENDENCIES
It requires mini-url-service to be running. 

## ENVIRONMENT VARIABLES
Set REACT_APP_POST_ENDPOINT environment variable to point to mini-url-service /shorten endpoint.

### EXAMPLE
REACT_APP_POST_ENDPOINT=http://localhost:3000/shorten

# HOW TO RUN
To run on local environment from project root execute command 'npm run start'